﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class ApartmaniController : Controller
    {
        private static ProcesorZaKorisnike procesorZaKorisnike;

        static ApartmaniController()
        {
            procesorZaKorisnike = ProcesorZaKorisnike.Instance;
        }
        // GET: Apartmani
        public ActionResult Modifikacija(int id)
        {
            ViewBag.Apartman = procesorZaKorisnike.PronadjiApartmanSaOdredjenimId(id);

            return View("ApartmanModifikacija");
        }

        public ActionResult BrisanjeApartmana(int id)
        {
            procesorZaKorisnike.ObrisiApartmanDomacina(id);

            return RedirectToAction("Index", "Home");
        }

        //ModifikujApartman
        public ActionResult ModifikujApartman(TipApartmana TipApartmana, int BrojSoba, int BrojGostiju, double GeografskaSirina, double GeografskaDuzina, string UlicaIBroj, string NaseljenoMesto, int PostanskiBroj, string Datumi, List<HttpPostedFileBase> Slike, double CenaPoNoci, string VremeZaPrijavu, string VremeZaOdjavu, Status Status, int Id, List<int> Sadrzaj)
        {
            string[] datumi = Datumi.Split(',');
            List<DateTime> datumii = new List<DateTime>();
            foreach (var datum in datumi)
            {
                DateTime ret;
                if (DateTime.TryParse(datum, out ret))
                {
                    datumii.Add(ret);
                }
            }

            List<string> putanje = new List<string>();
            if (Slike[0] != null)
            {
                Slike.ForEach(slika =>
                {
                    string putanjaSlike = Path.Combine(Server.MapPath("~/Slike"), slika.FileName);
                    putanje.Add(putanjaSlike);
                    slika.SaveAs(putanjaSlike);
                });
            }

            List<SadrzajApartmana> sadrzajApartmana = new List<SadrzajApartmana>();
            sadrzajApartmana = procesorZaKorisnike.MapirajIdNaSadrzajAparmana(Sadrzaj);

            List<SadrzajApartmana> izbaceniVecDodati = new List<SadrzajApartmana>();
            Apartman stariApartman = procesorZaKorisnike.PronadjiApartmanSaOdredjenimId(Id);

            foreach (var sadrzaj in sadrzajApartmana)
            {
                if(!DaLiSeSadrzajNalaziUStaromApartmanu(sadrzaj.ID, stariApartman.ListaSadrzajaApatmana))
                {
                    izbaceniVecDodati.Add(sadrzaj);
                }
            }
            foreach (var sadrzaj in stariApartman.ListaSadrzajaApatmana)
            {
                izbaceniVecDodati.Add(sadrzaj);
            }

            Korisnik dom = null;  
            foreach (var kor in procesorZaKorisnike.Korisnici)
            {
                if(kor.Uloga == Uloga.Domacin)
                {
                    foreach (var a in kor.ApartmaniZaIzdavanje)
                    {
                        if(a.Id == Id)
                        {
                            dom = kor;
                            break;
                        }
                    }
                }                
            }

            Apartman apartman = new global::Apartman()
            {
                Id = Id,
                TipApartmana = TipApartmana,
                BrojSoba = BrojSoba,
                BrojGostiju = BrojGostiju,
                Lokacija = new Lokacija(GeografskaSirina, GeografskaDuzina, new Adresa(UlicaIBroj, NaseljenoMesto, PostanskiBroj)),
                DatumiZaIzdavanje = datumii,
                DostupnostPoDatumima = new List<Entry>(),
                Domacin = dom, 
                Komentari = stariApartman.Komentari,
                Slike = putanje,
                CenaPoNoci = CenaPoNoci,
                VremeZaPrijavu = VremeZaPrijavu,
                VremeZaOdjavu = VremeZaOdjavu,
                Status = Status,
                ListaRezervacija = new List<Rezervacija>(),
                ListaSadrzajaApatmana = izbaceniVecDodati,
                Obrisan = false
            };

            procesorZaKorisnike.ModifikujApartmanPostojecegDomacina(apartman);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult SadrzajiApartmana()
        {
            return View();
        }


        public ActionResult DodavanjeSadrzajaApartmana(string Naziv)
        {
            procesorZaKorisnike.DodajNoviSadrzajApartmana(Naziv);

            return RedirectToAction("SadrzajiApartmana");
        }

        private bool DaLiSeSadrzajNalaziUStaromApartmanu(int id, List<SadrzajApartmana> sadrzaj)
        {
            bool ret = false;
            foreach (var item in sadrzaj)
            {
                if(item.ID == id)
                {
                    ret = true;
                    break;
                }
            }

            return ret;
        }

        public ActionResult ModifikacijaSadrzajaApartmana(string Naziv, int Id)
        {
            SadrzajApartmana noviSadrzaj = new SadrzajApartmana(Id, Naziv);
            procesorZaKorisnike.ModifikacijaSadrzajaApartmana(noviSadrzaj);

            return RedirectToAction("SadrzajiApartmana");
        }

        public ActionResult BrisanjeSadrzajaApartmana(int Id)
        {
            procesorZaKorisnike.BrisanjeSadrzajaApartmana(Id);

            return RedirectToAction("SadrzajiApartmana");
        }

        public ActionResult KorisnickiPregledApartmana(int idApartmana)
        {
            ViewBag.Apartman = procesorZaKorisnike.PronadjiApartmanSaOdredjenimId(idApartmana);

            return View("ApartmanPrikaz");
        }

        public ActionResult Komentar(string ime, string prezime, int ocena, string text, int id)
        {
            Komentar kom = new global::Komentar(new Korisnikk(ime, prezime), text, ocena);
            procesorZaKorisnike.DodajKomentar(id, kom);

            return RedirectToAction("Index", "Home");
        }
    }
}