﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Projekat.Controllers
{
    [RoutePrefix("ajax/prijava")]
    public class PrijavaIRegistracijaRESTController : ApiController
    {
        private static ProcesorZaKorisnike procesorZaKorisnike;

        static PrijavaIRegistracijaRESTController()
        {
            procesorZaKorisnike = ProcesorZaKorisnike.Instance;
        }

        [HttpPost]
        [Route("Korisnici")]
        public string Korisnici(Prijava prijava)
        {
            string odgovor = procesorZaKorisnike.DaLiJeKorisnikRegistrovan(prijava);
            return odgovor;
        }

        [HttpPost]
        [Route("KorisnickoPostojanje")]
        public string KorisnickoPostojanje(Prijava prijava)
        {
            bool odgovor = procesorZaKorisnike.DaLiKorisnikVecPostojiUListiRegistrovanih(prijava.KorisnickoIme);

            return odgovor ? "Korisnik sa datim imenom vec postoji" : "";
        }
    }
}
