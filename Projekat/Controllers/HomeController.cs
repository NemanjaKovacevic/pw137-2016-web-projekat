﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            
            if(TempData["PozivIzPretragaKontrolera"] == null)
            {
                ViewBag.Apartmani = ProcesorZaKorisnike.Instance.SviApartmani();
            }
            else
            {
                ViewBag.Apartmani = ProcesorZaKorisnike.Instance.PreuzmiPrikazApartmana();
            }

            return View("Apartmani");
        }
    }
}
