﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class PrijavaIRegistracijaController : Controller
    {
        private static ProcesorZaKorisnike procesorZaKorisnike;

        static PrijavaIRegistracijaController()
        {
            procesorZaKorisnike = ProcesorZaKorisnike.Instance;
        }

        // GET: PrijavaIRegistracija
        public ActionResult Prijava()
        {
            return View();
        }

        public ActionResult Registracija()
        {
            return View();
        }

        public ActionResult Profil()
        {
            return View();
        }

        public ActionResult PrijaviKorisnika(Prijava prijava)
        {
            Korisnik korisnik = procesorZaKorisnike.PronadjiKorisnika(prijava.KorisnickoIme);

            Korisnik k = (Korisnik)Session["Korisnik"];
            if (k == null)
            {
                k = korisnik;
                Session["Korisnik"] = k;
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult OdjaviKorisnika()
        {
            Korisnik korisnik = null;
            korisnik = (Korisnik)Session["Korisnik"];
            if (korisnik != null)
            {
                Session.Abandon();
            }

            return RedirectToAction("Index", "Home");
        }


        public ActionResult RegistrujGosta(KorisnikVM korisnik)
        {
            Korisnik zaRegistraciju = new Korisnik(korisnik.KorisnickoIme, korisnik.Lozinka, korisnik.Ime, korisnik.Prezime, korisnik.Pol, Uloga.Gost);
            procesorZaKorisnike.DodajKorisnika(zaRegistraciju);

            return RedirectToAction("Prijava");
        }

        public ActionResult RegistrujDomacina(KorisnikVM domacin)
        {
            Korisnik zaRegistraciju = new Korisnik(domacin.KorisnickoIme, domacin.Lozinka, domacin.Ime, domacin.Prezime, domacin.Pol, Uloga.Domacin);
            procesorZaKorisnike.DodajKorisnika(zaRegistraciju);

            return RedirectToAction("PregledSvihKorisnika");
        }

        public ActionResult IzmenaPodataka(KorisnikVM korisnik, string StaroKorisnickoIme)
        {
            Korisnik stariKorisnik = procesorZaKorisnike.PronadjiKorisnikaPoKorisnickomImenu(StaroKorisnickoIme);
            Korisnik zaDodavanje = new Korisnik(korisnik.KorisnickoIme, korisnik.Lozinka, korisnik.Ime, korisnik.Prezime, korisnik.Pol, stariKorisnik.Uloga);
            
            zaDodavanje.ApartmaniZaIzdavanje = stariKorisnik.ApartmaniZaIzdavanje;
            zaDodavanje.IznajmljeniApartmani = stariKorisnik.IznajmljeniApartmani;
            zaDodavanje.Rezervacije = stariKorisnik.Rezervacije;

            //procesorZaKorisnike.ObrisiKorisnika(StaroKorisnickoIme);
            //procesorZaKorisnike.DodajKorisnika(zaDodavanje);
            procesorZaKorisnike.IzmeniKorisnika(zaDodavanje, StaroKorisnickoIme);

            Session["Korisnik"] = zaDodavanje;

            return RedirectToAction("Index", "Home");
        }

        //PregledSvihKorisnika
        public ActionResult PregledSvihKorisnika()
        {
            if(TempData["Korisnici"] == null)
            {
                ViewBag.Korisnici = procesorZaKorisnike.Korisnici;
            }
            else
            {
                ViewBag.Korisnici = TempData["Korisnici"];
            }
            
            return View("Korisnici");
        }

    }
}