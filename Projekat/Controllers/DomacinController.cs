﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class DomacinController : Controller
    {
        private static ProcesorZaKorisnike procesorZaKorisnike;

        static DomacinController()
        {
            procesorZaKorisnike = ProcesorZaKorisnike.Instance;
        }
        // GET: Domacin
        public ActionResult Apartman()
        {
            ViewBag.Apartman = (Apartman)TempData["ApartmanZaModifikaciju"];
            return View();
        }

        public ActionResult KreirajApartman(TipApartmana TipApartmana, int BrojSoba, int BrojGostiju, double GeografskaSirina, double GeografskaDuzina, string UlicaIBroj, string NaseljenoMesto, int PostanskiBroj, string Datumi, List<HttpPostedFileBase> Slike, double CenaPoNoci, string VremeZaPrijavu, string VremeZaOdjavu, Status Status, List<int> Sadrzaj)
        {
            string[] datumi = Datumi.Split(',');
            List<DateTime> datumii = new List<DateTime>();
            foreach (var datum in datumi)
            {
                DateTime ret;
                if(DateTime.TryParse(datum, out ret))
                {
                    datumii.Add(ret);
                }
            }

            List<string> putanje = new List<string>();
            if(Slike[0] != null)
            {
                Slike.ForEach(slika =>
                {
                    string putanjaSlike = Path.Combine(Server.MapPath("~/Slike"), slika.FileName);
                    putanje.Add(putanjaSlike);
                    slika.SaveAs(putanjaSlike);
                });
            }

            List<SadrzajApartmana> sadrzajApartmana = new List<SadrzajApartmana>();
            sadrzajApartmana = procesorZaKorisnike.MapirajIdNaSadrzajAparmana(Sadrzaj);

            Apartman apartman = new global::Apartman()
            {
                Id = IdGenerator.GetId(),
                TipApartmana = TipApartmana,
                BrojSoba = BrojSoba,
                BrojGostiju = BrojGostiju,
                Lokacija = new Lokacija(GeografskaSirina, GeografskaDuzina, new Adresa(UlicaIBroj, NaseljenoMesto, PostanskiBroj)),
                DatumiZaIzdavanje = datumii,
                DostupnostPoDatumima = new List<Entry>(),
                Domacin = (Korisnik)Session["Korisnik"],
                Komentari = new List<Komentar>(),
                Slike = putanje,
                CenaPoNoci = CenaPoNoci,
                VremeZaPrijavu = VremeZaPrijavu,
                VremeZaOdjavu = VremeZaOdjavu,
                Status = Status,
                ListaRezervacija = new List<Rezervacija>(),
                ListaSadrzajaApatmana = sadrzajApartmana,
                Obrisan = false
            };

            procesorZaKorisnike.DodajApartmanPostojecemDomacinu(apartman, apartman.Domacin.KorisnickoIme);

            return RedirectToAction("Index", "Home");
        }
    }
}