﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class PretragaController : Controller
    {
        private static ProcesorZaKorisnike procesorZaKorisnike;

        static PretragaController()
        {
            procesorZaKorisnike = ProcesorZaKorisnike.Instance;
        }

        public ActionResult PretragaKorisnika(string Uloga, string Pol, string KorisnickoIme)
        {
            //Filtriraju se prikazani podaci, a kod pretrage se pretražuju svi podaci.
            
            List<Korisnik> sviKorisnici = procesorZaKorisnike.Korisnici;
            List<Korisnik> saUlogom = new List<Korisnik>();
            List<Korisnik> saPolom = new List<Korisnik>();
            List<Korisnik> saKorisnickimImenom = new List<Korisnik>();

            if (Uloga == "Administrator" || Uloga == "Domacin" || Uloga == "Gost")
            {
                Uloga uloga;
                Enum.TryParse(Uloga, out uloga);
                sviKorisnici.Where(i => i.Uloga == uloga).ToList().ForEach(i => saUlogom.Add(i));
            }
            else
            {
                saUlogom = sviKorisnici;
            }

            if(Pol == "Muski" || Pol == "Zenski")
            {
                Pol pol;
                Enum.TryParse(Pol, out pol);
                saUlogom.Where(i => i.Pol == pol).ToList().ForEach(i => saPolom.Add(i));
            }
            else
            {
                saPolom = saUlogom;
            }

            if(!string.IsNullOrEmpty(KorisnickoIme))
            {
                saPolom.Where(i => i.KorisnickoIme.ToLower().Contains(KorisnickoIme.Trim().ToLower())).ToList().ForEach(i => saKorisnickimImenom.Add(i));
            }
            else
            {
                saKorisnickimImenom = saPolom;
            }

            TempData["Korisnici"] = saKorisnickimImenom;

            return RedirectToAction("PregledSvihKorisnika", "PrijavaIRegistracija");
        }

        public ActionResult PretragaApartmana(string dolazak, string odlazak, string lokacija, string cenaMin, string cenaMax, string sobaMin, string sobaMax, string soba)
        {
            List<Apartman> apartmani = procesorZaKorisnike.SviApartmani();
            List<Apartman> saPogodnimVremenom = new List<Apartman>();
            List<Apartman> saLokacijom = new List<Apartman>();
            List<Apartman> saCenom = new List<Apartman>();
            List<Apartman> saOpsegomBrojaSoba = new List<Apartman>();
            List<Apartman> saTacnimBrojemSoba = new List<Apartman>();

            if(!string.IsNullOrEmpty(dolazak) && !string.IsNullOrEmpty(odlazak))
            {
                DateTime dol;
                DateTime odl;
                DateTime.TryParse(dolazak, out dol);
                DateTime.TryParse(odlazak, out odl);

                var datumiBoravka = new List<DateTime>();

                for (var dt = dol; dt <= odl; dt = dt.AddDays(1))
                {
                    datumiBoravka.Add(dt);
                }

                foreach (var apartman in apartmani)
                {
                    if(DaLiApartmanSadrziSveDatumeBoravkaKorisnika(datumiBoravka,apartman))
                    {
                        saPogodnimVremenom.Add(apartman);
                    }
                }

            }
            else
            {
                saPogodnimVremenom = apartmani;
            }

            if(!string.IsNullOrEmpty(lokacija))
            {
                foreach (var apartman in saPogodnimVremenom)
                {
                    if (apartman.Lokacija.Adresa.NaseljenoMesto.ToLower().Trim().Contains(lokacija.ToLower().Trim()) ||
                         apartman.Lokacija.Adresa.UlicaIBroj.ToLower().Trim().Contains(lokacija.ToLower().Trim()))
                    {
                        saLokacijom.Add(apartman);
                    }
                }
            }
            else
            {
                saLokacijom = saPogodnimVremenom;
            }

            if(!string.IsNullOrEmpty(cenaMin) && !string.IsNullOrEmpty(cenaMax))
            {
                double min, max;
                double.TryParse(cenaMax, out max);
                double.TryParse(cenaMin, out min);

                foreach (var apartman in saLokacijom)
                {
                    if(apartman.CenaPoNoci <= max && apartman.CenaPoNoci >= min)
                    {
                        saCenom.Add(apartman);
                    }
                }
            }
            else
            {
                saCenom = saLokacijom;
            }

            if(!string.IsNullOrEmpty(sobaMin) && !string.IsNullOrEmpty(sobaMax))
            {
                int min, max;
                int.TryParse(sobaMax, out max);
                int.TryParse(sobaMin, out min);

                foreach (var apartman in saCenom)
                {
                    if (apartman.BrojSoba <= max && apartman.BrojSoba >= min)
                    {
                        saOpsegomBrojaSoba.Add(apartman);
                    }
                }
            }
            else
            {
                saOpsegomBrojaSoba = saCenom;
            }

            if(!string.IsNullOrEmpty(soba))
            {
                int brojSoba;
                int.TryParse(soba, out brojSoba);

                foreach (var apartman in saOpsegomBrojaSoba)
                {
                    if (apartman.BrojSoba == brojSoba)
                    {
                        saTacnimBrojemSoba.Add(apartman);
                    }
                }
            }
            else
            {
                saTacnimBrojemSoba = saOpsegomBrojaSoba;
            }

            //TempData["Apartmanii"] = saTacnimBrojemSoba;
            TempData["PozivIzPretragaKontrolera"] = true;
            procesorZaKorisnike.DodeliVrednostPrikazuApartmana(saTacnimBrojemSoba);
           
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SortiranjeApartmana(string sortiranje)
        {
            List<Apartman> apartmani = procesorZaKorisnike.PreuzmiPrikazApartmana();
            List<Apartman> sortiranaLista;
            
            if(sortiranje == "rastuce")
            {
                sortiranaLista = apartmani.OrderBy(o => o.CenaPoNoci).ToList();
            }
            else if(sortiranje == "opadajuce")
            {
                sortiranaLista = apartmani.OrderBy(o => o.CenaPoNoci).Reverse().ToList();
            }
            else
            {
                sortiranaLista = apartmani;
            }

            TempData["PozivIzPretragaKontrolera"] = true;

            procesorZaKorisnike.DodeliVrednostPrikazuApartmana(sortiranaLista);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult FiltriranjeApartmana(List<int> Sadrzaj, string TipApartmana, string Status)
        {
            List<Apartman> prikazApartmana = procesorZaKorisnike.PreuzmiPrikazApartmana();
            List<Apartman> saSadrzajem = new List<Apartman>();
            List<Apartman> saTipomApartmana = new List<Apartman>();
            List<Apartman> saStatusom = new List<Apartman>();

            if(Sadrzaj != null && Sadrzaj.Count != 0)
            {
                foreach (var apartman in prikazApartmana)
                {
                    if(DaLiApartmanSadrziSveSadrzajeApartmana(apartman, Sadrzaj))
                    {
                        saSadrzajem.Add(apartman);
                    }
                }
            }
            else
            {
                saSadrzajem = prikazApartmana;
            }

            if(TipApartmana == "CeoApartman" || TipApartmana == "Soba")
            {
                TipApartmana tip;
                Enum.TryParse(TipApartmana, out tip);

                foreach (var apartman in saSadrzajem)
                {
                    if (apartman.TipApartmana == tip) 
                    {
                        saTipomApartmana.Add(apartman);
                    }
                }
            }
            else
            {
                saTipomApartmana = saSadrzajem;
            }

            if(Status == "Aktivan" ||  Status == "Neaktivan")
            {
                Status status;
                Enum.TryParse(Status, out status);
                foreach (var apartman in saTipomApartmana)
                {
                    if (apartman.Status == status)
                    {
                        saStatusom.Add(apartman);
                    }
                }
            }
            else
            {
                saStatusom = saTipomApartmana;
            }

            TempData["PozivIzPretragaKontrolera"] = true;
            procesorZaKorisnike.DodeliVrednostPrikazuApartmana(saStatusom);

            return RedirectToAction("Index", "Home");
        }

     
        
        private bool DaLiApartmanSadrziSveSadrzajeApartmana(Apartman apartman, List<int> sadrzaji)
        {
            bool ret = true;

            foreach (var sadrzajId in sadrzaji)
            {
                if(apartman.ListaSadrzajaApatmana.Find(sadrzaj => sadrzaj.ID == sadrzajId) == null)
                {
                    ret = false;
                    break;
                }
            }

            return ret;
        }

        private bool DaLiApartmanSadrziSveDatumeBoravkaKorisnika(List<DateTime> datumi, Apartman apartman)
        {
            bool ret = true;
            foreach (var datum in datumi)
            {
                if(!DaLiApartmanSadrziDatumBoravkaKorisnika(apartman, datum))
                {
                    ret = false;
                    break;
                }
            }

            return ret;
        }

        private bool DaLiApartmanSadrziDatumBoravkaKorisnika(Apartman apartman, DateTime datum)
        {
            bool ret = false;
            foreach (var d in apartman.DatumiZaIzdavanje)
            {
                if(d.Year == datum.Year && d.Month == datum.Month && d.Day == datum.Day)
                {
                    ret = true;
                    break;
                }
            }
            return ret;
        }
    }
}