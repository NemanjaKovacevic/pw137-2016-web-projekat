﻿
function registracija() {
    var ret = true;

    var korisnickoIme = document.forms["forma1"].KorisnickoIme.value;
    var lozinka = document.forms["forma1"].Lozinka.value;
    var ime = document.forms["forma1"].Ime.value;
    var prezime = document.forms["forma1"].Prezime.value;
    var pol = document.forms["forma1"].Pol.value;


    if (korisnickoIme == "") {
        $("#ki").html("Morate uneti korisnicko ime.");
        ret = false;
    }
    if (lozinka == "") {
        $("#l").html("Morate uneti lozinku.");
        ret = false;
    }
    if (ime == "") {
        $("#i").html("Morate uneti ime.");
        ret = false;
    }
    if (prezime == "") {
        $("#p").html("Morate uneti prezime.");
        ret = false;
    }
    if (pol != "Muski" && pol != "Zenski") {
        $("#pol").html("Morate odabrati pol.");
        ret = false;
    }


    var objekat = {
        "KorisnickoIme": korisnickoIme,
        "Lozinka": lozinka
    };

    var zaSlanje = JSON.stringify(objekat);

    if (ret == true) {
        $.ajax({
            url: "/ajax/prijava/KorisnickoPostojanje",
            type: "post",  //tip metode koju gadjamo
            data: zaSlanje,  //objekat koji se prosledjuje serveru
            contentType: "application/json",   //govorimo da je podatak koji se salje tipa string
            dataType: "json",   //tip podatka koji se ocekuje od servera
            async: false,
            success: function (data) {
                $("#ki").html(data);
                if (data == "Korisnik sa datim imenom vec postoji")
                    ret = false;
            },

            error: function (err) {
                alert("Doslo je do greske : " + err);
            }
        });
    }

    if (ret == true) {
        alert("Registracija korisnika " + ime + " " + prezime + " je uspesno izvrsena.");
    }

    return ret;
}