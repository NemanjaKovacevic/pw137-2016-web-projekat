﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class SadrzajApartmana
{
    public int ID { get; set; }
    public string Naziv { get; set; }
    public bool Obrisan { get; set; }

    public SadrzajApartmana(int id, string naziv)
    {
        ID = id;
        Naziv = naziv;
        Obrisan = false;
    }

    public SadrzajApartmana()
    {

    }
}