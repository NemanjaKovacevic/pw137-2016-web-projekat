﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Adresa
{
    public string UlicaIBroj { get; set; }
    public string NaseljenoMesto { get; set; }
    public int PostanskiBroj { get; set; }

    public Adresa()
    {

    }

    public Adresa(string ulicaIBroj, string naseljenoMesto, int postanskiBroj)
    {
        UlicaIBroj = ulicaIBroj;
        NaseljenoMesto = naseljenoMesto;
        PostanskiBroj = postanskiBroj;
    }
}
