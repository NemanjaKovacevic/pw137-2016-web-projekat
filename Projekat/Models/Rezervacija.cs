﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Rezervacija
{
    public Apartman Apartman { get; set; }
    public DateTime PocetniDatumRezervacije { get; set; }
    public int BrojNocenja { get; set; }
    public double UkupnaCena { get; set; }
    public Korisnik Gost { get; set; }
    public StatusRezervacije Status { get; set; }

    public Rezervacija(Apartman apartman, DateTime pocetniDatumRezervacije, int brojNocenja, double ukupnaCena, Korisnik gost, StatusRezervacije status)
    {
        Apartman = apartman;
        PocetniDatumRezervacije = pocetniDatumRezervacije;
        BrojNocenja = brojNocenja;
        UkupnaCena = ukupnaCena;
        Gost = gost;
        Status = status;
    }

    public Rezervacija()
    {

    }
}

public enum StatusRezervacije
{
    Kreirana,
    Odbijena,
    Odustanak,
    Prihvacena,
    Zavrsena
}
