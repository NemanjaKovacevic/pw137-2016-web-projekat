﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Korisnikk
{
    public string Ime { get; set; }
    public string Prezime { get; set; }

    public Korisnikk()
    {

    }

    public Korisnikk(string ime, string prezime)
    {
        Ime = ime;
        Prezime = prezime;
    }
}
