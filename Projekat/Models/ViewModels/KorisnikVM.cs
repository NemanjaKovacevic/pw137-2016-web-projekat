﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class KorisnikVM
{
    public string KorisnickoIme { get; set; }
    public string Lozinka { get; set; }
    public string Ime { get; set; }
    public string Prezime { get; set; }
    public Pol Pol { get; set; }
}
