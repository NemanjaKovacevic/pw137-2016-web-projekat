﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

//[Serializable, XmlRoot("Korisnik")]
public class Korisnik
{
    public string KorisnickoIme { get; set; }
    public string Lozinka { get; set; }
    public string Ime { get; set; }
    public string Prezime { get; set; }
    public Pol Pol { get; set; }
    public Uloga Uloga { get; set; }
    public List<Apartman> ApartmaniZaIzdavanje { get; set; }
    public List<Apartman> IznajmljeniApartmani { get; set; }
    public List<Rezervacija> Rezervacije { get; set; }

    public Korisnik(string korisnickoIme, string lozinka, string ime, string prezime, Pol pol, Uloga uloga)
    {
        KorisnickoIme = korisnickoIme;
        Lozinka = lozinka;
        Ime = ime;
        Prezime = prezime;
        Pol = pol;
        Uloga = uloga;
        if(uloga == Uloga.Domacin)
        {
            ApartmaniZaIzdavanje = new List<Apartman>();
            IznajmljeniApartmani = null;
            Rezervacije = null;
        }
        else if(uloga == Uloga.Gost)
        {
            ApartmaniZaIzdavanje = null;
            IznajmljeniApartmani = new List<Apartman>();
            Rezervacije = new List<Rezervacija>();
        }
        else
        {
            ApartmaniZaIzdavanje = null;
            IznajmljeniApartmani = null;
            Rezervacije = null;
        }
        
    }

    public Korisnik()
    {
        ApartmaniZaIzdavanje = null;
        IznajmljeniApartmani = null;
        Rezervacije = null;
    }

}

public enum Uloga
{
    Administrator,
    Domacin,
    Gost
}

public enum Pol
{
    Muski,
    Zenski
}