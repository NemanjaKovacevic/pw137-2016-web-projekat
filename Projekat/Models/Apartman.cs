﻿
using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

public class Apartman
{
    public int Id { get; set; }
    public TipApartmana TipApartmana { get; set; }
    public int BrojSoba { get; set; }
    public int BrojGostiju { get; set; }
    public Lokacija Lokacija { get; set; }
    public List<DateTime> DatumiZaIzdavanje { get; set; }
    public List<Entry> DostupnostPoDatumima { get; set; } = new List<Entry>();

    [XmlIgnore]
    [ScriptIgnore]
    public Korisnik Domacin { get; set; }
    public List<Komentar> Komentari { get; set; }
    public List<string> Slike { get; set; }
    public double CenaPoNoci { get; set; }
    public string VremeZaPrijavu { get; set; }
    public string VremeZaOdjavu { get; set; }
    public Status Status { get; set; } = Status.Neaktivan;
    public List<SadrzajApartmana> ListaSadrzajaApatmana { get; set; }
    public List<Rezervacija> ListaRezervacija { get; set; }
    public bool Obrisan { get; set; } = false;

    public Apartman()
    {

    }
}

public enum TipApartmana
{
    CeoApartman,
    Soba
}

public enum Status
{
    Aktivan,
    Neaktivan
}

