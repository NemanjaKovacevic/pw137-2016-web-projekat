﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Entry
{
    public DateTime Key;
    public bool Value;
    public Entry()
    {
    }

    public Entry(DateTime key, bool value)
    {
        Key = key;
        Value = value;
    }
}
