﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class IdGenerator
{
    private static int id;

    static IdGenerator()
    {
        id = PronadjiNajveciId();
    }

   

    private static int PronadjiNajveciId()
    {
        int ret;

        //List<Apartman> apartmani = (List<Apartman>)HttpContext.Current.Application["Apartmani"];
        List<Korisnik> korisnici = ProcesorZaKorisnike.Instance.Korisnici;
        List<Apartman> apartmani = new List<Apartman>();
        foreach (var korisnik in korisnici)
        {
            if(korisnik.Uloga == Uloga.Domacin)
            {
                foreach (var apartman in korisnik.ApartmaniZaIzdavanje)
                {
                    //nema provere da li je obrisan ili nije
                    apartmani.Add(apartman);
                }
            }
        }

        if (apartmani == null || apartmani.Count == 0)
        {
            ret = 0;
        }
        else
        {
            int max = apartmani[0].Id;
            foreach (var item in apartmani)
            {
                if (item.Id > max)
                    max = item.Id;
            }

            ret = max + 1;
        }

        return ret;

    }


    public static int GetId()
    {
        return id++;
    }
}
