﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public static class IdGeneratorSadrzaj
{
    private static int id;

    static IdGeneratorSadrzaj()
    {
        id = PronadjiNajveciId();
    }

    private static int PronadjiNajveciId()
    {
        List<SadrzajApartmana> sadrzaj = (List<SadrzajApartmana>)HttpContext.Current.Application["Sadrzaj"];

        int ret;

        if (sadrzaj == null || sadrzaj.Count == 0)
        {
            ret = 0;
        }
        else
        {
            int max = sadrzaj[0].ID;
            foreach (var item in sadrzaj)
            {
                if (item.ID > max)
                    max = item.ID;
            }

            ret = max + 1;
        }

        return ret;
    }


    public static int GetId()
    {
        return id++;
    }
}
