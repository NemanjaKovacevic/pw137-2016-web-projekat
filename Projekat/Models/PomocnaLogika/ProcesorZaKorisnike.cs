﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

public class ProcesorZaKorisnike
{
    private List<Korisnik> korisnici;
    private static ProcesorZaKorisnike instance;
    private XmlSerializer serializer;
    private XmlSerializer deserializer;
    private string path;
    public List<Korisnik> Korisnici { get => korisnici; }
    private SadrzajApartmanaFajl SadrzajApartmanaFajl;


    public static ProcesorZaKorisnike Instance
    {
        get
        {
            if (instance == null)
                instance = new ProcesorZaKorisnike();

            return instance;
        }
    }

    
    private ProcesorZaKorisnike()
    {
        XmlRootAttribute xRoot = new XmlRootAttribute();
        xRoot.ElementName = "korisnik";
        xRoot.IsNullable = true;
        serializer = new XmlSerializer(typeof(List<Korisnik>), xRoot);
        
        deserializer = new XmlSerializer(typeof(List<Korisnik>), xRoot);

        path = HttpRuntime.AppDomainAppPath + "App_Data\\Korisnici.xml";

        SadrzajApartmanaFajl = new SadrzajApartmanaFajl();

        UcitajIzFajla();
        UcitajAdministratoreIzFajla();
        ObnoviListuApartmana();
        
    }

    #region UpisICitanjeIzXMLFajla
    private void UcitajIzFajla()
    {
        if (File.Exists(path))
        {
            using (StreamReader file = new StreamReader(path))
            {
                korisnici = (List<Korisnik>)deserializer.Deserialize(file);
                file.Close();
            }

            if (korisnici == null || korisnici.Count == 0)
            {
                korisnici = new List<Korisnik>();
            }

            ObnoviDomacinaUnutarApartmana();
            HttpContext.Current.Application["Korisnici"] = korisnici;
            DodajSveApartmane();
        }
        else
        {
            korisnici = new List<Korisnik>();
            HttpContext.Current.Application["Korisnici"] = korisnici;
        }
        
    }

    private void ObnoviDomacinaUnutarApartmana()
    {
        foreach (var korisnik in korisnici)
        {
            if(korisnik.Uloga == Uloga.Domacin)
            {
                foreach (var apartman in korisnik.ApartmaniZaIzdavanje)
                {
                    apartman.Domacin = korisnik;
                }
            }
            
        }
    }
    private void UpisiUFajl()
    {
        using (StreamWriter file = new StreamWriter(path))
        {
           
            List<Korisnik> bezAdministratora = new List<Korisnik>();
            korisnici.ForEach(kor =>
            {
                if (kor.Uloga != Uloga.Administrator)
                    bezAdministratora.Add(kor);
            });
            serializer.Serialize(file, bezAdministratora);
            file.Close();
        }
    }
    #endregion

    #region DodavanjeApartmanaUGlobalnuPromenljivu
    private void DodajSveApartmane()
    {
        korisnici.ForEach(korisnik => DodajApartmaneDomacina(korisnik));
    }

    private void DodajApartman(Apartman apartman)
    {
        List<Apartman> apartmani = (List<Apartman>)HttpContext.Current.Application["Apartmani"];
        if(apartmani == null)
        {
            apartmani = new List<Apartman>();
        }
        apartmani.Add(apartman);
        
        HttpContext.Current.Application["Apartmani"] = apartmani;
    }

    private void DodajApartmaneDomacina(Korisnik domacin)
    {
        domacin.ApartmaniZaIzdavanje.ForEach(apartman => 
        {
            if (!apartman.Obrisan)
                DodajApartman(apartman);
        });
    }
    #endregion

    public void IzmeniKorisnika(Korisnik noviKorisnik, string staroKorisnickoIme)
    {
        for (int i = 0; i < korisnici.Count; i++)
        {
            if(korisnici[i].KorisnickoIme == staroKorisnickoIme)
            {
                korisnici[i] = noviKorisnik;
                break;
            }
        }

        if(noviKorisnik.Uloga != Uloga.Administrator)
        {
            if (noviKorisnik.Uloga == Uloga.Domacin)
            {
                ObnoviDomacinaUnutarApartmana();
            }

            HttpContext.Current.Application["Korisnici"] = korisnici;
            ObnoviListuApartmana();
            UpisiUFajl();
        }
        else
        {
            UpisiUFajlSveAdministratore();
        }
        
    }

    private void UpisiUFajlSveAdministratore()
    {
        string zaUpisati = "";
        foreach (var korisnik in korisnici)
        {
            if(korisnik.Uloga == Uloga.Administrator)
            {
                zaUpisati += korisnik.KorisnickoIme + " " +
                            korisnik.Lozinka + " " +
                            korisnik.Ime + " " +
                            korisnik.Prezime + " " +
                            korisnik.Pol + Environment.NewLine;
            }
        }

        string putanja = HttpRuntime.AppDomainAppPath + "App_Data\\Administratori.txt";

        File.WriteAllText(putanja, zaUpisati);
    }
    public void DodajKorisnika(Korisnik korisnik)
    {
        korisnici.Add(korisnik);
        HttpContext.Current.Application["Korisnici"] = korisnici;
        UpisiUFajl();
        if (korisnik.Uloga == Uloga.Domacin)
            DodajApartmaneDomacina(korisnik); // ???
    }

    /*public void ObrisiKorisnika(string korisnickoIme)
    {
        var kor = PronadjiKorisnikaPoKorisnickomImenu(korisnickoIme);
        if(kor != null)
        {
            if(kor.Uloga == Uloga.Domacin)
            {
                korisnici.Remove(kor);
                ObrisiKorisnikaIzXmlFajla(korisnickoIme);
                DodajSveApartmane();
            }
            else
            {
                korisnici.Remove(kor);
                ObrisiKorisnikaIzXmlFajla(korisnickoIme);
            }
            HttpContext.Current.Application["Korisnici"] = korisnici;
        }
    }*/

    /*private void ObrisiKorisnikaIzXmlFajla(string korisnickoIme)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(path);

        var korisniciXml = doc.GetElementsByTagName("Korisnik");
        foreach (XmlNode item in korisniciXml)
        {
            if(item.FirstChild.InnerXml == korisnickoIme)
            {
                item.ParentNode.RemoveChild(item);
                break;
            }
        }

        doc.Save(path);
    }*/

    
    public void DodajApartmanPostojecemDomacinu(Apartman apartman, string korisnickoIme)
    {
        Korisnik domacin = PronadjiKorisnikaPoKorisnickomImenu(korisnickoIme);
        domacin.ApartmaniZaIzdavanje.Add(apartman);
        HttpContext.Current.Application["Korisnici"] = korisnici;
        UpisiUFajl();
        ObnoviListuApartmana();
    }

    public void ModifikujApartmanPostojecegDomacina(Apartman noviApartman)
    {
        Korisnik domacin = PronadjiKorisnikaPoKorisnickomImenu(noviApartman.Domacin.KorisnickoIme);
        for (int i = 0; i < domacin.ApartmaniZaIzdavanje.Count; i++)
        {
            if(domacin.ApartmaniZaIzdavanje[i].Id == noviApartman.Id)
            {
                domacin.ApartmaniZaIzdavanje[i] = noviApartman;
                break;
            }
        }
        HttpContext.Current.Application["Korisnici"] = korisnici;
        UpisiUFajl();
        ObnoviListuApartmana();
    }

    public void ObrisiApartmanDomacina(int id)
    {
        Apartman apartman = PronadjiApartmanSaOdredjenimId(id);
        apartman.Obrisan = true;
        HttpContext.Current.Application["Korisnici"] = korisnici;
        UpisiUFajl();
        ObnoviListuApartmana();
    }

    private void ObnoviListuApartmana()
    {
        List<Apartman> apartmani = new List<Apartman>();
        korisnici.ForEach(korisnik =>
        {
            if (korisnik.Uloga == Uloga.Domacin)
            {
                korisnik.ApartmaniZaIzdavanje.ForEach(apartman =>
                {
                    if (!apartman.Obrisan)
                    {
                        apartmani.Add(apartman);
                    }
                });
            }
        });

        HttpContext.Current.Application["Apartmani"] = apartmani;
        HttpContext.Current.Application["ApartmaniPrikaz"] = apartmani;
    }
    
    #region TrazenjeKorisnikaPoUslovu
    public Korisnik PronadjiKorisnikaPoKorisnickomImenu(string korisnickoIme)
    {
        return korisnici.Find(i => i.KorisnickoIme == korisnickoIme);
    }

   
    public List<Korisnik> PronadjiKorisnikePoUlozi(Uloga uloga)
    {
        List<Korisnik> ret = new List<Korisnik>();
        foreach (var item in korisnici)
        {
            if (item.Uloga == uloga)
            {
                ret.Add(item);
            }
        }

        return ret;
    }

    public List<Korisnik> PronadjiKorisnikePoPolu(Pol pol)
    {
        List<Korisnik> ret = new List<Korisnik>();
        foreach (var item in korisnici)
        {
            if (item.Pol == pol)
            {
                ret.Add(item);
            }
        }

        return ret;
    }
    #endregion

    #region UcitavanjeAdministratoraIzFajla
    private void UcitajAdministratoreIzFajla()
    {
        string path = HttpRuntime.AppDomainAppPath + "App_Data\\Administratori.txt";
        using (StreamReader file = new StreamReader(path))
        {
            string line = "";
            while ((line = file.ReadLine()) != null)
            {
                Korisnik admin = ParsirajLinijuIzUlazneDatoteke(line);
                DodajAdministratora(admin);
            }

            file.Close();
        }
    }

    private Korisnik ParsirajLinijuIzUlazneDatoteke(string linija)
    {
        string[] delovi = linija.Split(' ');

        Pol pol;
        if (delovi[4] == "Muski")
            pol = Pol.Muski;
        else
            pol = Pol.Zenski;

        Korisnik ret = new Korisnik(delovi[0], delovi[1], delovi[2], delovi[3], pol, Uloga.Administrator);

        return ret;
    }

    private void DodajAdministratora(Korisnik korisnik)
    {
        korisnici.Add(korisnik);
        HttpContext.Current.Application["Korisnici"] = korisnici;
        //bez upisa u fajl
    }
    #endregion  

    #region MetodeStareKlase
    public string DaLiJeKorisnikRegistrovan(Prijava prijava)
    {
        Korisnik korisnik = korisnici.Find(i => i.KorisnickoIme == prijava.KorisnickoIme);
        if (korisnik != null)
        {
            if (korisnik.Lozinka != prijava.Lozinka)
            {
                return "Uneli ste neispravnu lozinku.";
            }
            return "Prijava je uspesno izvrsena.";
        }
        else
            return "Korisnik sa datim korisnickim imenom ne postoji.";
    }

    public bool DaLiKorisnikVecPostojiUListiRegistrovanih(string korisnickoIme)
    {
        Korisnik korisnik = korisnici.Find(i => i.KorisnickoIme == korisnickoIme);
        if (korisnik != null)
            return true;
        else
            return false;

    }

    public Korisnik PronadjiKorisnika(string korisnickoIme)
    {
        return korisnici.Find(i => i.KorisnickoIme == korisnickoIme);
    }
    #endregion


    public Apartman PronadjiApartmanSaOdredjenimId(int id)
    {
        Apartman ret = null;

        foreach (var korisnik in korisnici)
        {
            if (korisnik.Uloga == Uloga.Domacin)
            {
                foreach (var apartman in korisnik.ApartmaniZaIzdavanje)
                {
                    if (apartman.Id == id)
                    {
                        ret = apartman;
                        break;
                    }
                }
            } 
        }

        return ret;

        
    }

    public List<SadrzajApartmana> MapirajIdNaSadrzajAparmana(List<int> multpleId)
    {
        List<SadrzajApartmana> ret = new List<SadrzajApartmana>();
        List<SadrzajApartmana> savSadrzaj = (List<SadrzajApartmana>)HttpContext.Current.Application["Sadrzaj"];

        if(multpleId != null)
        {
            foreach (var id in multpleId)
            {
                foreach (var sadrzaj in savSadrzaj)
                {
                    if (id == sadrzaj.ID)
                    {
                        ret.Add(sadrzaj);
                    }
                }
            }
        }
        
        return ret;
    }

    public List<SadrzajApartmana> DodajNoviSadrzajApartmana(string naziv)
    {
        List<SadrzajApartmana> sadrzaj = (List<SadrzajApartmana>)HttpContext.Current.Application["Sadrzaj"];
        SadrzajApartmana sa = new SadrzajApartmana(IdGeneratorSadrzaj.GetId(), naziv);
        sadrzaj.Add(sa);
        HttpContext.Current.Application["Sadrzaj"] = sadrzaj;
        SadrzajApartmanaFajl.UpisiUFajl();

        return sadrzaj;
    }

    public void BrisanjeSadrzajaApartmana(int idSadrzaja)
    {
        List<SadrzajApartmana> sadrzaj = (List<SadrzajApartmana>)HttpContext.Current.Application["Sadrzaj"];
        foreach (var s in sadrzaj)
        {
            if(s.ID == idSadrzaja)
            {
                sadrzaj.Remove(s);
                break;
            }
        }

        foreach (var korisnik in korisnici)
        {
            if(korisnik.Uloga == Uloga.Domacin)
            {
                foreach (var apartman in korisnik.ApartmaniZaIzdavanje)
                {
                    foreach (var sadrz in apartman.ListaSadrzajaApatmana)
                    {
                        if(sadrz.ID == idSadrzaja)
                        {
                            apartman.ListaSadrzajaApatmana.Remove(sadrz);
                            break;
                        }
                    }
                }
            }
        }

        HttpContext.Current.Application["Sadrzaj"] = sadrzaj;
        SadrzajApartmanaFajl.UpisiUFajl();
        HttpContext.Current.Application["Korisnici"] = korisnici;
        UpisiUFajl();
        ObnoviListuApartmana();
    }

    public void ModifikacijaSadrzajaApartmana(SadrzajApartmana sadrz)
    {
        List<SadrzajApartmana> sadrzaj = (List<SadrzajApartmana>)HttpContext.Current.Application["Sadrzaj"];
        for (int i = 0; i < sadrzaj.Count; i++)
        {
            if (sadrzaj[i].ID == sadrz.ID)
            {
                sadrzaj[i] = sadrz;
                break;
            }
        }

        foreach (var korisnik in korisnici)
        {
            if (korisnik.Uloga == Uloga.Domacin)
            {
                foreach (var apartman in korisnik.ApartmaniZaIzdavanje)
                {
                    for (int i = 0; i < apartman.ListaSadrzajaApatmana.Count; i++)
                    {
                        if (sadrz.ID == apartman.ListaSadrzajaApatmana[i].ID)
                        {
                            apartman.ListaSadrzajaApatmana[i] = sadrz;
                            break;
                        }
                    }
                }
            }
        }

        HttpContext.Current.Application["Sadrzaj"] = sadrzaj;
        SadrzajApartmanaFajl.UpisiUFajl();
        HttpContext.Current.Application["Korisnici"] = korisnici;
        UpisiUFajl();
        ObnoviListuApartmana();
    }

    public List<Apartman> SviApartmani()
    {
        return (List<Apartman>)HttpContext.Current.Application["Apartmani"];
    }

    public void DodeliVrednostPrikazuApartmana(List<Apartman> apartmanii)
    {
        HttpContext.Current.Application["ApartmaniPrikaz"] = apartmanii;
    }

    public List<Apartman> PreuzmiPrikazApartmana()
    {
        return (List<Apartman>)HttpContext.Current.Application["ApartmaniPrikaz"];
    }

    public void DodajKomentar(int idAp, Komentar kom)
    {
        foreach (var kor in korisnici)
        {
            if(kor.Uloga == Uloga.Domacin)
            {
                foreach (var apart in kor.ApartmaniZaIzdavanje)
                {
                    if(apart.Id == idAp)
                    {
                        apart.Komentari.Add(kom);
                        break;
                    }
                }
            }
            
        }

        UpisiUFajl();
    }
}
