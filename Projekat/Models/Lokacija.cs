﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Lokacija
{
    public double GeografskaSirina { get; set; }
    public double GeografskaDuzina { get; set; }
    public Adresa Adresa { get; set; }

    public Lokacija()
    {

    }

    public Lokacija(double geografskaSirina, double geografskaDuzina, Adresa adresa)
    {
        GeografskaSirina = geografskaSirina;
        GeografskaDuzina = geografskaDuzina;
        Adresa = adresa;
    }
}

