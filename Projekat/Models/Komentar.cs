﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Komentar
{
    public Korisnikk Gost { get; set; }
    public string Text { get; set; }
    public int Ocena { get; set; }

    public Komentar(Korisnikk gost, string text, int ocena)
    {
        Gost = gost;
        Text = text;
        Ocena = ocena;
    }

    public Komentar()
    {
        
    }
}
