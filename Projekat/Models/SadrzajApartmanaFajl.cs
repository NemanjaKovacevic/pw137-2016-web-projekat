﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;


public class SadrzajApartmanaFajl
{
    private string path;
    private XmlSerializer serializer;
    private XmlSerializer deserializer;
    List<SadrzajApartmana> sadrzaj;

    public SadrzajApartmanaFajl()
    {
        serializer = new XmlSerializer(typeof(List<SadrzajApartmana>));

        deserializer = new XmlSerializer(typeof(List<SadrzajApartmana>));

        path = HttpRuntime.AppDomainAppPath + "App_Data\\SadrzajApartmana.xml";

        UcitajIzFajla();
    }

    private void UcitajIzFajla()
    {
        sadrzaj = (List<SadrzajApartmana>)HttpContext.Current.Application["Sadrzaj"];


        if (File.Exists(path))
        {
            using (StreamReader file = new StreamReader(path))
            {
                sadrzaj = (List<SadrzajApartmana>)deserializer.Deserialize(file);
                file.Close();
            }

            if (sadrzaj == null || sadrzaj.Count == 0)
            {
                sadrzaj = new List<SadrzajApartmana>();
            }

            HttpContext.Current.Application["Sadrzaj"] = sadrzaj;
        }
        else
        {
            sadrzaj = new List<SadrzajApartmana>();
            HttpContext.Current.Application["Sadrzaj"] = sadrzaj;
        }
    }

    public void UpisiUFajl()
    {
        using (StreamWriter file = new StreamWriter(path))
        {
            sadrzaj = (List<SadrzajApartmana>)HttpContext.Current.Application["Sadrzaj"];
            serializer.Serialize(file, sadrzaj);
            file.Close();
        }
    }
}
